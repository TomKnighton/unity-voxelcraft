﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Chunk
{
    MeshRenderer MeshRenderer;
    MeshFilter MeshFilter;
    GameObject ChunkObject;
    MeshCollider MeshCollider;

    int vertexIndex = 0;
    List<Vector3> vertices = new List<Vector3>();
    List<int> triangles = new List<int>();
    List<Vector2> uvs = new List<Vector2>();

    BlockType[,,] VoxelMap = new BlockType[VoxelSettings.ChunkWidth, VoxelSettings.ChunkHeight, VoxelSettings.ChunkWidth];

    private World _World;

    public Chunk(World _world)
    {
        _World = _world;

        ChunkObject = new GameObject();
        MeshRenderer = ChunkObject.AddComponent<MeshRenderer>();
        MeshFilter = ChunkObject.AddComponent<MeshFilter>();
        MeshCollider = ChunkObject.AddComponent<MeshCollider>();
        MeshCollider.cookingOptions = MeshColliderCookingOptions.CookForFasterSimulation;
        ChunkObject.transform.SetParent(_World.transform);
        MeshRenderer.material = _World.Material;
        ChunkObject.name = "Chunk";

        PopulateVoxelMap();
        CreateMeshData();
        CreateMesh();
    }

    private void PopulateVoxelMap()
    {
        for (int y = 0; y < VoxelSettings.ChunkHeight; y++)
        {
            for (int x = 0; x < VoxelSettings.ChunkWidth; x++)
            {
                for (int z = 0; z < VoxelSettings.ChunkWidth; z++)
                {
                    VoxelMap[x, y, z] = InternalGameSettings.GameBlocks[1];
                }
            }
        }
    }

    private void CreateMeshData()
    {
        for (int y = 0; y < VoxelSettings.ChunkHeight; y++)
        {
            for (int x = 0; x < VoxelSettings.ChunkWidth; x++)
            {
                for (int z = 0; z < VoxelSettings.ChunkWidth; z++)
                {
                    AddVoxelDataToChunk(new Vector3(x, y, z));
                }
            }
        }
    }

    private bool CheckVoxel(Vector3 pos)
    {
        int x = Mathf.FloorToInt(pos.x); int y = Mathf.FloorToInt(pos.y); int z = Mathf.FloorToInt(pos.z);
        if (x < 0 || x > VoxelSettings.ChunkWidth - 1 || y < 0 || y > VoxelSettings.ChunkHeight - 1 || z < 0 || z > VoxelSettings.ChunkWidth - 1)
            return false;
        return VoxelMap[x,y,z].IsSolid;
    }

    private void AddVoxelDataToChunk(Vector3 pos)
    {
        for (int p = 0; p < 6; p++)
        {
            if (!CheckVoxel(pos + InternalGameSettings.FaceChecks[p]))
            {
                for (int i = 0; i < 6; i++)
                {
                    BlockType blockType = VoxelMap[(int)pos.x, (int)pos.y, (int)pos.z];

                    vertices.Add(pos + InternalGameSettings.VoxelVertices[InternalGameSettings.VoxelTriangles[p, 0]]);
                    vertices.Add(pos + InternalGameSettings.VoxelVertices[InternalGameSettings.VoxelTriangles[p, 1]]);
                    vertices.Add(pos + InternalGameSettings.VoxelVertices[InternalGameSettings.VoxelTriangles[p, 2]]);
                    vertices.Add(pos + InternalGameSettings.VoxelVertices[InternalGameSettings.VoxelTriangles[p, 3]]);

                    AddTextureToVoxel(blockType.Texture.GetTextureID(p));

                    triangles.Add(vertexIndex);
                    triangles.Add(vertexIndex + 1);
                    triangles.Add(vertexIndex + 2);
                    triangles.Add(vertexIndex + 2);
                    triangles.Add(vertexIndex + 1);
                    triangles.Add(vertexIndex + 3);
                    vertexIndex += 4;

                }
            }
           
        }
    }


    private void AddTextureToVoxel(int TextureId)
    {
        float y = (TextureId / InternalGameSettings.TextureAtlasSizeInBlocks) * InternalGameSettings.NormalisedBlockTextureSize;
        float x = (TextureId - (y * InternalGameSettings.TextureAtlasSizeInBlocks)) * InternalGameSettings.NormalisedBlockTextureSize;

        y = 1f - y - InternalGameSettings.NormalisedBlockTextureSize;

        uvs.Add(new Vector2(x, y));
        uvs.Add(new Vector2(x, y + InternalGameSettings.NormalisedBlockTextureSize));
        uvs.Add(new Vector2(x + InternalGameSettings.NormalisedBlockTextureSize, y));
        uvs.Add(new Vector2(x + InternalGameSettings.NormalisedBlockTextureSize, y + InternalGameSettings.NormalisedBlockTextureSize));
    }

    private void CreateMesh()
    {
        Mesh mesh = new Mesh();
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.uv = uvs.ToArray();
        mesh.RecalculateNormals();
        MeshFilter.mesh = mesh;
        MeshCollider.sharedMesh = mesh;
    }

}
