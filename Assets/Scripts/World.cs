﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour
{
    public Material Material;
    public List<BiomeTypes> Biomes;


    void Start()
    {
        new Chunk(this);
    }

}


[System.Serializable]
public class BlockType
{
  
    

    public string BlockName { get; set; }
    public int BlockId { get; set; }
    public bool IsSolid { get; set; }
    public Texture Texture { get; set; }

   
}

[System.Serializable]
public class Texture
{
    public string TextureName;
    public int BackFaceTexture;
    public int FrontFaceTexture;
    public int TopFaceTexture;
    public int BottomFaceTexture;
    public int LeftFaceTexture;
    public int RightFaceTexture;

    public int GetTextureID(int faceIndex)
    {

        switch (faceIndex)
        {
            case 0:
                return BackFaceTexture;
            case 1:
                return FrontFaceTexture;
            case 2:
                return TopFaceTexture;
            case 3:
                return BottomFaceTexture;
            case 4:
                return LeftFaceTexture;
            case 5:
                return RightFaceTexture;
            default:
                Debug.Log("Error in GetTextureID; invalid face index");
                return 0;

        }
    }
}

[System.Serializable]

public class BiomeTypes
{
    public string BiomeName;
    public int BiomeId;
    public List<BlockType> Blocks;
} 
