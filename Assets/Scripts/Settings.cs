﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;




public static class VoxelSettings
{
    public static readonly int ChunkWidth = 5;
    public static readonly int ChunkHeight = 5;
}
public static class InternalGameSettings
{
    public static readonly List<Texture> BlockTextures = new List<Texture>()
    {
        new Texture { TextureName = "Bedrock", BackFaceTexture = 15, FrontFaceTexture = 15, BottomFaceTexture = 15, LeftFaceTexture = 15, RightFaceTexture = 15, TopFaceTexture = 15},
        new Texture { TextureName = "Grass", BackFaceTexture = 3, FrontFaceTexture = 3, BottomFaceTexture = 2, LeftFaceTexture = 3, RightFaceTexture = 3, TopFaceTexture = 0},
        new Texture { TextureName = "Dirt", BackFaceTexture = 2, FrontFaceTexture = 2, BottomFaceTexture = 2, LeftFaceTexture = 2, RightFaceTexture = 2, TopFaceTexture = 2},
        new Texture { TextureName = "Stone", BackFaceTexture = 1, FrontFaceTexture = 1, BottomFaceTexture = 1, LeftFaceTexture = 1, RightFaceTexture = 1, TopFaceTexture = 1},
    };

    public static readonly List<BlockType> GameBlocks = new List<BlockType>
    {
        new BlockType { BlockName = "Air", BlockId = 0, IsSolid = false, Texture = BlockTextures.Where(bt => bt.TextureName == "Bedrock").FirstOrDefault() },
        new BlockType { BlockName = "Bedrock", BlockId = 1, IsSolid = true, Texture = BlockTextures.Where(bt => bt.TextureName == "Bedrock").FirstOrDefault() },
        new BlockType { BlockName = "Grass", BlockId = 2, IsSolid = true, Texture = BlockTextures.Where(bt => bt.TextureName == "Grass").FirstOrDefault() },
        new BlockType { BlockName = "Dirt", BlockId = 3, IsSolid = true, Texture = BlockTextures.Where(bt => bt.TextureName == "Dirt").FirstOrDefault() },
        new BlockType { BlockName = "Stone", BlockId = 4, IsSolid = true, Texture = BlockTextures.Where(bt => bt.TextureName == "Stone").FirstOrDefault() },
    };

    public static readonly int TextureAtlasSizeInBlocks = 16;
    public static readonly float NormalisedBlockTextureSize = 1f / TextureAtlasSizeInBlocks;

    public static readonly Vector3[] VoxelVertices = new Vector3[8] {

        new Vector3(0.0f, 0.0f, 0.0f),
        new Vector3(1.0f, 0.0f, 0.0f),
        new Vector3(1.0f, 1.0f, 0.0f),
        new Vector3(0.0f, 1.0f, 0.0f),
        new Vector3(0.0f, 0.0f, 1.0f),
        new Vector3(1.0f, 0.0f, 1.0f),
        new Vector3(1.0f, 1.0f, 1.0f),
        new Vector3(0.0f, 1.0f, 1.0f),

    };

    public static readonly int[,] VoxelTriangles = new int[6, 4] {

        {0, 3, 1, 2}, // Back Face
		{5, 6, 4, 7}, // Front Face
		{3, 7, 2, 6}, // Top Face
		{1, 5, 0, 4}, // Bottom Face
		{4, 7, 0, 3}, // Left Face
		{1, 2, 5, 6} // Right Face

	};

    public static readonly Vector3[] FaceChecks = new Vector3[6] {

        new Vector3(0.0f, 0.0f, -1.0f),
        new Vector3(0.0f, 0.0f, 1.0f),
        new Vector3(0.0f, 1.0f, 0.0f),
        new Vector3(0.0f, -1.0f, 0.0f),
        new Vector3(-1.0f, 0.0f, 0.0f),
        new Vector3(1.0f, 0.0f, 0.0f)

};

    public static readonly Vector2[] VoxelUVs = new Vector2[4] {

        new Vector2 (0.0f, 0.0f),
        new Vector2 (0.0f, 1.0f),
        new Vector2 (1.0f, 0.0f),
        new Vector2 (1.0f, 1.0f)

    };
}
